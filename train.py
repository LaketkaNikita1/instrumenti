from sklearn.metrics import accuracy_score, f1_score, confusion_matrix
from classifier_iris import Classifier_Iris
import matplotlib.pyplot as plt
import seaborn as sns

if __name__ == '__main__':

    model = Classifier_Iris()
    model.training_model()
    model.predict_model()
    with open('predict.txt', 'r') as f:
        pred_y = f.read().split('\n')
    with open('test_y.txt', 'r') as f:
        test_y = f.read().split('\n')

    accuracy = accuracy_score(test_y, pred_y)
    f1 = f1_score(test_y, pred_y, average='weighted')
    with open('metrics.txt', 'w') as f:
        f.write(f"Accuracy: {accuracy:.2f}\n")
        f.write(f"F1 Score: {f1:.2f}\n")

    # Выводим метрики
    print(f"Accuracy: {accuracy:.2f}")
    print(f"F1 Score: {f1:.2f}")

    # Строим матрицу ошибок (Confusion Matrix)
    conf_matrix = confusion_matrix(test_y, pred_y)

    # Визуализируем матрицу ошибок
    plt.figure(figsize=(8, 6))
    sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='Blues')
    plt.xlabel('Предсказанные классы')
    plt.ylabel('Истинные классы')
    plt.title('Матрица ошибок')
    plt.savefig('plot.png')
