import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

import pandas as pd
from sklearn.preprocessing import LabelEncoder

class Classifier_Iris:
    clf = None
    iris = None
    y_pred = None
    n_estimators = None
    test_y = None
    def __init__(self, n_estimators=100):

        self.n_estimators = n_estimators
        # Load the iris dataset
        self.iris = pd.read_csv("https://www.alvinang.sg/s/iris_dataset.csv")

        # Create a label encoder
        le = LabelEncoder()

        # Fit the label encoder to the species column
        le.fit(self.iris["species"])

        # Transform the species column
        self.iris["species"] = le.transform(self.iris["species"])
    def training_model(self):
        # Target
        y = self.iris["species"]

        # Features
        X = self.iris[["sepal_length", "sepal_width", "petal_length", "petal_width"]]

        #Split the data into a training set and a testing set.
        train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=0.2)
        # Creating a Random Forest model and training it on training data
        self.clf = RandomForestClassifier(n_estimators=self.n_estimators, random_state=42)
        self.clf.fit(train_X, train_y)

        # Predicting classes for text data
        self.y_pred = self.clf.predict(test_X)
        self.test_y = test_y

    def predict_model(self):
        np.savetxt('predict.txt', self.y_pred, fmt='%d')
        np.savetxt('test_y.txt', self.test_y, fmt='%d')
